#!/bin/bash

if [ $# == '0' ] 
then
echo "USAGE: $0 file"
exit 1
fi
file=$1
dir=$(realpath $(dirname $0))
player="mplayer"
list=mplayer
hexdump -ve '1/1 "%.2x"' ${file} | fold -w1 | while read half_byte
do
mpv "./Dtmf${half_byte}.wav"
list="$list ./Dtmf${half_byte}.wav"
done
sleep 1
echo $list
mplayer $list
